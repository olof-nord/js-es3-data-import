# js-es3-data-import

Small demo to, using vanilla JavaScript, parse and display an Excel file using no frameworks and only ES3 language features.

## Dependencies

Turns out Excel data is binary, and as such cannot be easily parsed.

For that, the [XLXS](https://github.com/SheetJS/sheetjs) library sheetjs proved to be a fitting option.

## Run

Just open the [index.html](public/index.html) file in any browser: there are two example Excel files in the repo already.
The demo is also deployed to [GitLab pages](https://olof-nord.gitlab.io/js-es3-data-import/).

Repository icon made by [Pixel perfect](https://www.flaticon.com/authors/pixel-perfect) found at [Flaticon](https://www.flaticon.com).
