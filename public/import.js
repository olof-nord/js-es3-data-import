function importFile() {
    var fileUpload = document.getElementById("fileUpload");
    var file = fileUpload.files[0];
    var reader = new FileReader();

    if (fileUpload.files.length == 0) {
        return;
    }

    reader.onloadend = function() {
        var arrayBuffer = reader.result;

        var options = { type: 'array' };
        var workbook = XLSX.read(arrayBuffer, options);

        var sheetName = workbook.SheetNames;
        var sheet = workbook.Sheets[sheetName];
        var sheetAsJSON = XLSX.utils.sheet_to_json(sheet, { header:1 });

        exportFile(sheetAsJSON);

    };

    reader.readAsArrayBuffer(file);

}

function exportFile(sheetAsJSON) {
    var table = document.getElementById("output");

    for (var i = 0; i < sheetAsJSON.length; i++) {
        var row = document.createElement("tr");

        if(i == 0) {
            populateRow(sheetAsJSON[i], row, "th")
        } else {
            populateRow(sheetAsJSON[i], row, "td")
        }
        table.appendChild(row);
    }
}

function populateRow(input, row, rowType) {
    for (var i = 0; i < input.length; i++) {
        var tableCell = document.createElement(rowType);
        tableCell.innerText = input[i]
        row.appendChild(tableCell);
    }

    return row;
}
